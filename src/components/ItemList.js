import React, { Component } from 'react'

import Item from './Item'

const ItemList = ({user, inc, dec, inc100, dec100, zero}) =>
    <ul>
        {user.map(userEl => <Item key={userEl.id} user={userEl} inc={inc} dec={dec}  inc100={inc100} dec100={dec100} zero={zero} />)}
    </ul>


export default ItemList