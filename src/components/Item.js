import React, {Component} from 'react'
import PropTypes from 'prop-types'

// class Item extends Component{
//     render(){
//         const{name, surname, age} = this.props.user
//
//         return <li>
//             <p>{name}</p>
//             <p>{surname}</p>
//             <p>{age}</p>
//         </li>
//     }
// }


const Item = ({user, inc, dec, inc100, dec100, zero}) =>
    <li>
        {user.name}
        {user.surname}
        {user.age}
        <p>user.id - {user.id}</p>
        <p>{user.count}</p>
        <button onClick={() => inc(user.id)} >+</button>
        <button onClick={() => dec(user.id)} >-</button>
        <button onClick={() => inc100(user.id)}>+100</button>
        <button onClick={() => dec100(user.id)}>-100</button>
        <button onClick={() => zero(user.id)}>0</button>
    </li>


export default Item