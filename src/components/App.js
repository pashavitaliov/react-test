import React, {Component} from 'react'
import {connect} from 'react-redux'
import ItemList from './ItemList'

import { bindActionCreators } from 'redux'
import * as UserActions from '../actions'

import {increment, decrement, increment100, decrement100, zero} from "../actions";
import Item from "./Item";

//
// class App extends Component {
//     render() {
//         const  {user}  = this.props
//
//         console.log('this.props', this.props);
//         console.log('this.props.user', user);
//         const listItems = user.map((user) =>
//             <Item key={user.age} user={user}/>
//         );
//         return <ul>
//                 {listItems}
//         </ul>
//     }
// }
//
// function mapStateToProps(state) {
//     return { user   : state.user123 }
// }
//
// export default connect(mapStateToProps)(App)

// const App = ({user}) =>
//     <ul> { user.map( el => <Item key={el.id} user={el}/> ) } </ul>
//
//
// function mapStateToProps(state) {
//     return { user : state.user123 }
// }
//
// export default connect(mapStateToProps)(App)

const App = ({user, increment, decrement, increment100, decrement100, zero}) =>
    <div>
        <ItemList user={user} inc={increment} dec={decrement} inc100={increment100} dec100={decrement100} zero={zero}/>
    </div>


function mapStateToProps(state)  {
    return { user : state.user123 }
}

const mapDispatchToProps = (dispatch)=> {
    return {
        increment: (id) => dispatch(increment(id)),
        increment100: (id) => dispatch(increment100(id)),
        decrement: (id) => dispatch(decrement(id)),
        decrement100: (id) => dispatch(decrement100(id)),
        zero: (id) => dispatch(zero(id))

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
