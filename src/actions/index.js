import * as type from '../constants'

export const increment = id => ({ type: type.INC, id})
export const decrement = id => ({ type: type.DEC, id})
export const increment100 = id => ({ type: type.INC100, id})
export const decrement100 = id => ({ type: type.DEC100, id})
export const zero = id => ({ type: type.ZERO, id})

