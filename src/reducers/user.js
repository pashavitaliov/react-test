import * as type from '../constants'

const initialState = [
    {
        name: 'name-1',
        surname: 'surname-1',
        age: 23,
        count: 0,
        id: 1
    }, {
        name: 'testName',
        surname: 'testSurname',
        age: 100,
        count: 100,
        id: 2
    }
];

export default function user(state = initialState, action) {
    switch (action.type) {
        case type.INC:
            return state.map(user =>
                user.id === action.id ?
                    {...user, count: user.count + 1} :
                    user
            )
        case type.INC100:
            return state.map(user =>
                user.id === action.id ?
                    {...user, count: user.count + 100} :
                    user
            )

        case type.DEC:
            return state.map(user =>
                user.id === action.id ?
                    {...user, count: user.count - 1} :
                    user
            )
        case type.DEC100:
            return state.map(user =>
                user.id === action.id ?
                    {...user, count: user.count - 100} :
                    user
            )

        case type.ZERO:
            return state.map(user =>
                user.id === action.id ?
                    {...user, count: 0} :
                    user
            )
        default:
            return state;
    }

}